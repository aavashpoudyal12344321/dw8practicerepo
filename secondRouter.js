import { Router } from "express";

// middleware is a function which has req, res, next
// next function is used to trigger another middleware
//  for one res=quest we must have only one response

// based on error it is divided into error middleware and without error middleware
// error middleware
// (err,req,res,next)
// next(value)

// without error middleware
// (req,res,next)
// next()


// Based on location
// route middleware
// application middleware



let secondRouter = Router()

secondRouter
.route("/")
.post((req,res,next)=>{
    console.log("I am middleware first")
    res.json("middleware 1")
    next();

}, 
(req,res,next)=>{
    console.log("I am middleware second")
    next();

},
(req,res,next)=>{
    console.log("I am middleware third")
    // next();
})
.get((req,res,next)=>{
    console.log("middleware 1")
    next("abc");
    },
    (err,req,res,next)=>{
    console.log("middleware 2")
    next("def");
},
    (req,res,next)=>{
    console.log("middleware 3")
    },
    (err,req,res,next)=>{
    console.log("middleware 4")
    }
)
.patch((req,res)=>{
    console.log("I am patch method")
})
.delete((req,res)=>{
    console.log("I am delete method")
})



export default secondRouter;