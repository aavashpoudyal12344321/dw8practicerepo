// express application (making)
// attach port

import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import secondRouter from "./secondRouter.js";
import connectToMongoDB from "./src/connectToDatabase/connectToMongoDB.js";
import studentRouter from "./src/router/studentRouter.js";
import teacherRouter from "./src/router/teacherRouter.js";
import bookRouter from "./src/router/bookRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import pupilRouter from "./src/router/pupilRouter.js";
import departmentRouter from "./src/router/departmentRouter.js";
import classRoomRouter from "./src/router/classRoomRouter.js";
import customerRouter from "./src/router/customerRouter.js";
import productRouter from "./src/router/productRouter.js";
import userRouter from "./src/router/userRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import chefRouter from "./src/router/chefRouter.js";
import cors from "cors";
import jwt from "jsonwebtoken"

// make express application
let expressApp = express()
connectToMongoDB()

// expressApp.use((req,res,next)=>{
//     console.log("I am app middleware 0")
//     next("ab")
// },
// (err,req,res,next)=>{
//     console.log("I am app middleware 1")
//     next()
// })
expressApp.use(json())
let port=8000
expressApp.use(cors());

 
expressApp.use("/",firstRouter) 
expressApp.use("/seconds",secondRouter) 
expressApp.use("/customers",customerRouter)
expressApp.use("/students",studentRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/books", bookRouter)
expressApp.use("/trainees", traineeRouter)
expressApp.use("/pupils", pupilRouter)
expressApp.use("/departments", departmentRouter)
expressApp.use("/classroom", classRoomRouter)
expressApp.use("/products", productRouter)
expressApp.use("/users", userRouter)
expressApp.use("/reviews", reviewRouter)
expressApp.use("/chef", chefRouter)
// attach port to application
expressApp.listen(port,()=>{
    console.log(`The express application is connected to port ${port} successfully.`)
});




// create API
// frontend request => backend response
// c = create => post method
// r = read => get method
// u = update => patch method
// d = delete => delete method
// connect our application to mongodb database
// Adding port

let infoObj = {
    name:"aavash",
    age:24
}

let secretKey ="hello1"

let expiryInfo = {
    expiresIn:"3d"
}

let Token = jwt.sign(infoObj, secretKey, expiryInfo);

console.log(Token)

