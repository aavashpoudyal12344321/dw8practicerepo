import mongoose from "mongoose";

let connectToMongoDB = async ()=>{
    try{
        await mongoose.connect("mongodb://0.0.0.0:27017/DW8")
           console.log("Application connected to mongodb database successfully") 
        }
        catch(error){
            console.log("Unable to connect to database")
        
        }
}

export default connectToMongoDB;







