import { Router } from "express"
import { UpdateClassRoom, createClassRoom, deleteClassRoom, readAllClassRoom, readSpecificClassRoom } from "../controller/classRoomController.js"

let classRoomRouter = Router()
classRoomRouter
.route("/")
.post(createClassRoom)
.get(readAllClassRoom)

classRoomRouter
.route("/id")
.get(readSpecificClassRoom)
.patch(UpdateClassRoom)
.delete(deleteClassRoom)

export default classRoomRouter