import { Router } from "express";

let firstRouter = Router()
firstRouter
.route("/")
.post((req,res)=>{
    console.log("I am post method.")
    res.json([1,2,3,4,5])
})
.get((req,res)=>{
    console.log("I am get method")
})
.patch((req,res)=>{
    console.log("I am patch method")
})
.delete((req,res)=>{
    console.log("I am delete method")
})

firstRouter
.route("/store")
.post((req,res)=>{
    console.log("Store created successfully.")
})
.get((req,res)=>{
    console.log("Store info read")
})
.patch((req,res)=>{
    console.log("Store info updated")
})
.delete((req,res)=>{
    console.log("Store is deleted")
})

firstRouter
.route("/store/chocolate")
.post((req,res)=>{
    console.log("Chocolate stored created.")
    res.json("Chocolate stored created successfully.")
})
.get((req,res)=>{
    res.json("Chocolate info read successfully.")
    console.log("Chocolate info read")
})
.patch((req,res)=>{
    res.json("Chocolate info updated successfully.")
    console.log("Chocolate info updated")
})
.delete((req,res)=>{
    res.json("Chocolate info deleted successfully.")
    console.log("Chocolate info is deleted")
})

firstRouter
.route("/trainee")
.post((req,res)=>{
    res.json({success:true,message:"School created successfully"})
})
.get((req,res)=>{
    res.json({success:true,message:"School read successfully"})
})
.patch((req,res)=>{
    res.json({success:true,message:"School updated successfully"})
})
.delete((req,res)=>{
    res.json({success:true,message:"School deleted successfully"})
})

firstRouter
.route("/bike")
.post((req,res)=>{
    res.json("bike post")
})
.get((req,res)=>{
    res.json("bike get")
})
.patch((req,res)=>{
    res.json("bike patch")
})
.delete((req,res)=>{
    res.json("bike delete")
})

export default firstRouter;