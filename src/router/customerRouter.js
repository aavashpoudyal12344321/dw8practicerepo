import { Router } from "express";

let customerRouter = Router()

customerRouter
.route("/")
.post((req,res)=>{
    res.json({
        success:true,
        message:"customer created successfully",
    })
})
.get((req,res)=>{
    res.json({success:true,message:"customer read successfully"})
})
.patch((req,res)=>{
    res.json({success:true,message:"customer updated successfully"})
})
.delete((req,res)=>{
    res.json({success:true,message:"customer deleted successfully"})
})

customerRouter
.route("/a")
.post((req,res)=>{
    res.json({
        success:true,
        message:"customer created successfully"+"/a",
    })
})
.get((req,res)=>{
    res.json({success:true,message:"customer read successfully"})
})
.patch((req,res)=>{
    res.json({success:true,message:"customer updated successfully"})
})
.delete((req,res)=>{
    res.json({success:true,message:"customer deleted successfully"})
})

customerRouter
.route("/a/:b/c")
.post((req,res)=>{
    res.json({
        success:true,
        message:"customer created successfully"+"/a/:b/c",
    })
})
.get((req,res)=>{
    res.json({success:true,message:"customer read successfully"})
})
.patch((req,res)=>{
    res.json({success:true,message:"customer updated successfully"})
})
.delete((req,res)=>{
    res.json({success:true,message:"customer deleted successfully"})
})

customerRouter
.route("/a/:b/c/:id1")
.post((req,res)=>{
    console.log(req.params)
    res.json({
        success:true,
        message:"customer created successfully"+"/a/:b/c/:id1",
    })
})
.get((req,res)=>{
    res.json({success:true,message:"customer read successfully"})
})
.patch((req,res)=>{
    res.json({success:true,message:"customer updated successfully"})
})
.delete((req,res)=>{
    res.json({success:true,message:"customer deleted successfully"})
})


export default customerRouter