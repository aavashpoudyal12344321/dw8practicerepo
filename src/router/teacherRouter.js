import { Router } from "express";
import { createTeacher, deleteTeacher, readAllTeacher, readSpecificTeacher, updateTeacher } from "../controller/teacherController.js";

let teacherRouter = Router()
teacherRouter
.route("/")
.post(createTeacher)
.get(readAllTeacher)

teacherRouter
.route("/id")
.get(readSpecificTeacher)
.patch(updateTeacher)
.delete(deleteTeacher)

export default teacherRouter