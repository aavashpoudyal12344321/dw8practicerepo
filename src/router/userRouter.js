import { Router } from "express";
import {  LoginUser, createUser, deleteUser, readAllUser, readSpecificUser, updateUser } from "../controller/userController.js";

let userRouter = Router()
userRouter
.route("/")
.post(createUser)
.get(readAllUser)

userRouter
.route("/login")
.post(LoginUser)


userRouter
.route("/:id")
.get(readSpecificUser)
.delete(deleteUser)
.patch(updateUser)




export default userRouter