import { Router } from "express";
import { createReview, deleteReview, readAllReview, readSpecificReview, updateReview } from "../controller/reviewController.js";

let reviewRouter = Router()
reviewRouter
.route("/")
.post(createReview)
.get(readAllReview)


reviewRouter
.route("/:id")
.get(readSpecificReview)
.delete(deleteReview)
.patch(updateReview)


export default reviewRouter