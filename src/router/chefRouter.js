import { Router } from "express";
import { createChef, deleteChef, readAllChef, readSpecificChef, updateChef } from "../controller/chefController.js";

let chefRouter = Router()
chefRouter
.route("/")
.post(createChef)
.get(readAllChef)

chefRouter
.route("/:id")
.get(readSpecificChef)
.patch(updateChef)
.delete(deleteChef)

export default chefRouter