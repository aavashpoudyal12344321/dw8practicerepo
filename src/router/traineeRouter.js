import { Router } from "express"
import { createTrainee, deleteTrainee, readAllTrainee, readSpecificTrainee, updateTrainee } from "../controller/traineeController.js"

let traineeRouter = Router()
traineeRouter
.route("/")
.post(createTrainee)
.get(readAllTrainee)

traineeRouter
.route("/:id")
.get(readSpecificTrainee)
.patch(updateTrainee)
.delete(deleteTrainee)

export default traineeRouter