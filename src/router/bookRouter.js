import { Router } from "express"
import { UpdateBook, createBook, deleteBook, readAllBook, readSpecificBook } from "../controller/bookController.js"

let bookRouter = Router()
bookRouter
.route("/")
.post(createBook)
.get(readAllBook)


bookRouter
.route("/:id")
.get(readSpecificBook)
.patch(UpdateBook)
.delete(deleteBook)

export default bookRouter