import { Router } from "express";
import { UpdateDepartment, createDepartment, deleteDepartment, readAllDepartment, readSpecificDepartment } from "../controller/departmentController.js";

let departmentRouter = Router()
departmentRouter
.route("/")
.post(createDepartment)
.get(readAllDepartment)

departmentRouter
.route("/id")
.get(readSpecificDepartment)
.patch(UpdateDepartment)
.delete(deleteDepartment)

export default departmentRouter