import { Router } from "express";
import { createPupil, deletePupil, readAllPupil, readSpecificPupil, updatePupil } from "../controller/pupilController.js";

let pupilRouter = Router()

pupilRouter
.route("/")
.post(createPupil)
.get(readAllPupil)

pupilRouter
.route("/:id")
.get(readSpecificPupil)
.patch(updatePupil)
.delete(deletePupil)

export default pupilRouter