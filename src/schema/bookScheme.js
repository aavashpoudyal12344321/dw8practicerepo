import { Schema } from "mongoose"

let bookSchema = Schema({
    name:{
        type : String,
        required : [true,"Book name is empty."],
    },
    author:{
        type: String,
        required : [true,"Author name is empty."],
    },
    price:{
        type: Number,
        required : [true,"Price is required."],
    },
    isAvailable:{
        type: Boolean,
        required: [true,"Book availability is not choose."],
    },
})

export default bookSchema