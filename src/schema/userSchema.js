import { Schema } from "mongoose";

let userSchema = Schema({
    name:{
        type : String,
        required : [true,"Name field is empty."]
    },
    email:{
        type: String,
        required : [true,"email is required."],
        unique: true
    },
    password:{
        type: String,
        required: [true,"Password is required"]
    },

})

export default userSchema