import { Schema } from "mongoose";

//  Schema is defining content of object
//  Model is defining content of array

let teacherSchema = Schema({
    name:{
        type : String,
        required : [true,"Name field is empty."]
    },
    age:{
        type: Number,
        required : [true,"Age is required."]
    },
    subject: {
        type: String,
        required : [true,"Subject is required"]
    },

})

export default teacherSchema