import { Schema } from "mongoose";

let reviewSchema = Schema({
    userId:{
        type : Schema.ObjectId,
        ref:"User",
        required : [true,"User ID field is empty."]
    },
    productId:{
        type: Schema.ObjectId,
        ref:"Product",
        required : [true,"Review Id is required."]
    },
    description:{
        type: String,
        required: [true,"Description is required"]
    },
})

export default reviewSchema