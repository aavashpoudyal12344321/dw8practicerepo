//  defining array

import { model } from "mongoose";
import bookSchema from "./bookScheme.js";
import classRoomSchema from "./classRoom.js";
import departmentSchema from "./department.js";
import pupilSchema from "./pupil.js";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import traineeSchema from "./trainee.js";
import productSchema from "./productSchema.js";
import userSchema from "./userSchema.js";
import reviewSchema from "./reviewSchema.js";
import chefSchema from "./chefSchema.js";

export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Book = model("Book", bookSchema);
export let Trainee = model("Trainee", traineeSchema)
export let Pupil = model("Pupil",pupilSchema)
export let Department = model("Department",departmentSchema)
export let ClassRoom = model("ClassRoom", classRoomSchema)
export let Product = model("Product", productSchema)
export let User = model("User", userSchema)
export let Review = model("Review", reviewSchema)
export let Chef = model("Chef", chefSchema)
