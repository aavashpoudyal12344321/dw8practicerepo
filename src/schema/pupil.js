import { Schema } from "mongoose";


let pupilSchema = Schema({
    name:{
        type : String,
        // uppercase:true,
        // lowercase:true,
        trim:true,
        required : [true,"Name field is empty."],
        minLength: [3,"Name length must be at least 3 characters"],
        maxLength: [25,"Name length must not exceed 20 characters"]
    },
    age:{
        type: Number,
        required : [true,"Age is required."]
    },
    phoneNumber:{
        type: Number,
        // min:[1000000000," Phone number must be of 10 character"],
        // max:9999999999,
        required : [true,"Phone number must be of 10 character"],
        validate : (value)=>{
            let str = value.toString()
            if(str.length!==10){
            throw new Error("Phone number must be of 10 character")
        }
        }
    },
    roll:{
        type: Number,
        min:[1,"roll number must be at least 1"],
        max:[100,"roll number must not exceed 100"],
        required : [true,"Roll is required."]
    },
    isMarried:{
        type: Boolean,
        required : [true,"isMarried is required."]
    },
    password: {
        type: String,
        required : [true,"Password must be entered"],
        validate: (value)=>{
            let regex = /^(?=[A-Za-z])(?=(.*\d))(?=(.*[^A-Za-z0-9]))[A-Za-z0-9!@#$%^&*()-_+={}[]|;:'",.<>?\\]{8,15}$/
            if(!regex.test(value)){
                throw new Error("Password schema unmatched")
            }
        }
    },
    email:{
        type: String,
        required : [true,"Email is required."],
        // unique:true,
        validate : (value)=>{
            if(value==="aavashpoudyal8287@gmail.com")
            throw new Error("This email is not authorized to register in our system")
        }
    },
    gender:{
        type: String,
        default: "male",
        required : [true,"Gender is required."]
    },
    dob:{
        type: Date,
        required : [true,"Dob is required."]
    },
    location:{
        country:{
            type:String,
        required : [true,"Country is required."],
    },
        exactLocation:{
            type:String,
        required : [true,"Exact location is required."],
    }
   
    },
    favTeacher:[
        {
        type:String,
        required : [true,"Favorite teacher is required."],},
        
    ],
    favSubject:[{
        bookName:{
            type:String,
        required : [true,"Favorite subject is required."],
    },
        bookAuthor:{
            type:String,
        required : [true,"Book Author is required."]
    }}
   
    ],

})

export default pupilSchema