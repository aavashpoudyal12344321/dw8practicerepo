import { Schema } from "mongoose";

let chefSchema = Schema({
    name:{
        type:String,
        required:[true,"Name is required"]
    },
    designation:{
        type:String,
        required:[true,"Designation is required"]
    },
    experience:{
        type:Number,
        required:[true,"Experience is required"]
    },
    speciality:{
        type:String,
        required:[true,"Speciality is required"]
    },
})

export default chefSchema