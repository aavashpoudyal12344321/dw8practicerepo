import { Schema } from "mongoose";

let productSchema = Schema({
    name:{
        type : String,
        required : [true,"Name field is empty."]
    },
    price:{
        type: Number,
        required : [true,"Price is required."]
    },
    quantity:{
        type: Number,
        required: [true,"Quantity is required"]
    },
    description:{
        type: String,
        required: [true,"Description is required"]
    },
})

export default productSchema