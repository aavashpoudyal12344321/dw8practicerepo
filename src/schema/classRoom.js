import { Schema } from "mongoose"
// name, numberOfBench, hasTv

let classRoomSchema = Schema({
    name:{
        type : String,
        required : [true,"name is empty."],
    },
    numberOfBench:{
        type: Number,
        required : [true,"numberOfBench name is empty."],
    },
    hasTV:{
        type: Boolean,
        required : [true,"TV info is required."],
    },
})

export default classRoomSchema