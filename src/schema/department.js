
import { Schema } from "mongoose"


//  Schema is defining content of object
//  Model is defining content of array

let departmentSchema = Schema({
    name:{
        type : String,
        required : [true,"Name field is empty."]
    },
    hod:{
        type: String,
        required : [true,"Hod is required."]
    },
    totalMember:{
        type: Number,
        required: [true,"Status required"]
    },
})

export default departmentSchema