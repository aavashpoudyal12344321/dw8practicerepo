import { Schema } from "mongoose"

let traineeSchema = Schema({
    name:{
        type : String,
        required : [true,"Name is empty."]
    },
    class:{
        type: String,
        required : [true,"Class is empty."]
    },
    faculty:{
        type: Number,
        required : [true,"Faculty is required."]
    },
})

export default traineeSchema