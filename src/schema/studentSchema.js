import { Schema } from "mongoose";

//  Schema is defining content of object
//  Model is defining content of array

let studentSchema = Schema({
    name:{
        type : String,
        required : [true,"Name field is empty."]
    },
    age:{
        type: Number,
        required : [true,"Age is required."]
    },
    isHappy:{
        type: Boolean,
        required: [true,"Status required"]
    },
})

export default studentSchema