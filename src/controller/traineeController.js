import { Trainee } from "../schema/model.js";

export let createTrainee =  async (req,res)=>{
    let data =  req.body;
    try {
     let result = await Trainee.create(data);
    res.json({
     success:true,
     message:"Trainee added successfully",
     data:result,
 })  
    } catch (error) {
     res.json({
         success:false,
         message: error.message
     })
    } 
 }

 export let readAllTrainee = async (req,res)=>{
    let data =  req.body;
   try {
    let result = await Trainee.find({});
   res.json({
    success:true,
    message:"Trainee read successfully",
    data:result,
})
   } catch (error) {
    res.json({
        success:false,
        message: error.message
    })
   }
}

export let readSpecificTrainee = async (req,res)=>{
    let id = req.params.id
    try {
        let result = await Trainee.findById(id);
       res.json({
        success:true,
        message:"Trainee read successfully",
        data:result,
    })
       } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
       }
}


export let updateTrainee = async (req,res)=>{
   let id= req.params.id
   let data = req.body
   try {
    let result = Trainee.findByIdAndUpdate(id,data, {new:true})
    res.json({
        success:true,
        message:"Trainee updated successfully",
        data:result
    })
   } catch (error) {
    res.json({
        success:false,
        message:error.message
    })
   }
}

export let deleteTrainee = async (req,res)=>{
    let id = req.params.id
    try {
        let result = Trainee.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"Trainee deleted successfully",
            data:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}