import { Review } from "../schema/model.js";

export let createReview =  async (req,res)=>{
    let data =  req.body;
    try {
     let result = await Review.create(data);
    res.json({
     success:true,
     message:"Review added successfully",
     data:result,
 })  
    } catch (error) {
     res.json({
         success:false,
         message: error.message
     })
    } 
 }

 export let readAllReview = async (req,res)=>{
    // let data =  req.body;
   try {

    let result = await Review.find({}).populate("userId", "name",).populate( "productId"," price");
   res.json({
    success:true,
    message:"Review read successfully",
    data:result,
})
   } catch (error) {
    res.json({
        success:false,
        message: error.message
    })
   }
}

export let readSpecificReview = async (req,res)=>{
    let id = req.params.id
    try {
        let result = await Review.findById(id);
       res.json({
        success:true,
        message:"Review read successfully",
        data:result,
    })
       } catch (error) {
        res.json({
            success:false,
            message: Error.message
        })
       }
}

// export let readSpecificReviewByName = async (req,res)=>{
//     try {
//         let result = await Review.find({name:"aavash"});
//        res.json({
//         success:true,
//         message:"Review read successfully",
//         data:result,
//     })
//        } catch (error) {
//         res.json({
//             success:false,
//             message: Error.message
//         })
//        }
// }

export let deleteReview = async (req,res)=>{
    let id = req.params.id
    
    try {
        let result = await Review.findByIdAndDelete(id)

        if (result===null){
            res.json({
                success:false,
                message:"Data not available to delete"
            })
        }
        else{
            res.json({
                success:true,
            message:"Review delete successfully",
            data:result,
            })

        }
        
    } catch (error) {
        res.json({
            success:false,
            message: Error.message
        })
        
    }
}


export let updateReview = async (req,res)=>{
    let id = req.params.id
    let data = req.body
    
    try {
        let result = await Review.findByIdAndUpdate(id, data, {new:true})

       
            res.json({
                success:true,
            message:"Review updated successfully",
            data:result,
            })

        
        
    } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
        
    }
}


