import { Student } from "../schema/model.js";

export let createStudent =  async (req,res)=>{
    let data =  req.body;
    try {
     let result = await Student.create(data);
    res.json({
     success:true,
     message:"Student added successfully",
     data:result,
 })  
    } catch (error) {
     res.json({
         success:false,
         message: Error.message
     })
    } 
 }

 export let readAllStudent = async (req,res)=>{
    // let data =  req.body;
   try {
    let page = parseInt(req.body.page) || 1;
    let itemsPerPage = 2;
    let skipCount = (page-1)*itemsPerPage;
    let result = await Student.find({}).skip(skipCount).limit(itemsPerPage);
   res.json({
    success:true,
    message:"Student read successfully",
    data:result,
})
   } catch (error) {
    res.json({
        success:false,
        message: error.message
    })
   }
}

export let readSpecificStudent = async (req,res)=>{
    let id = req.params.id
    try {
        let result = await Student.findById(id);
       res.json({
        success:true,
        message:"Student read successfully",
        data:result,
    })
       } catch (error) {
        res.json({
            success:false,
            message: Error.message
        })
       }
}

// export let readSpecificStudentByName = async (req,res)=>{
//     try {
//         let result = await Student.find({name:"aavash"});
//        res.json({
//         success:true,
//         message:"Student read successfully",
//         data:result,
//     })
//        } catch (error) {
//         res.json({
//             success:false,
//             message: Error.message
//         })
//        }
// }

export let deleteStudent = async (req,res)=>{
    let id = req.params.id
    
    try {
        let result = await Student.findByIdAndDelete(id)

        if (result===null){
            res.json({
                success:false,
                message:"Data not available to delete"
            })
        }
        else{
            res.json({
                success:true,
            message:"Student delete successfully",
            data:result,
            })

        }
        
    } catch (error) {
        res.json({
            success:false,
            message: Error.message
        })
        
    }
}


export let updateStudent = async (req,res)=>{
    let id = req.params.id
    let data = req.body
    
    try {
        let result = await Student.findByIdAndUpdate(id, data, {new:true})

       
            res.json({
                success:true,
            message:"Student updated successfully",
            data:result,
            })

        
        
    } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
        
    }
}


