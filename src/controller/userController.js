import { User } from "../schema/model.js";
import bcrypt from "bcrypt";
import { sendMail } from "../utils/sendMail.js";

export let createUser = async (req, res) => {
  let data = req.body;
  try {
    let password = data.password;
    let hashPassword = await bcrypt.hash(password, 10);

    data = {
      ...data,
      password: hashPassword,
    };

    let result = await User.create(data);

    await sendMail({
      from:'"Aavash" <aavashpoudyal12365t6gvhg tvgcvfgh j321@gmail.com>',
      to: [ `${data.email}`],
      subject : "Learning beckend email",
      html:"<p>This message is sent automatically from backend of my first system</p>"
    })
    res.json({
      success: true,
      message: "User added successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: Error.message,
    });
  }
};

export let readAllUser = async (req, res) => {
  // let data =  req.body;
  try {
    let result = await User.find({});
    res.json({
      success: true,
      message: "User read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificUser = async (req, res) => {
  let id = req.params.id;
  try {
    let result = await User.findById(id);
    res.json({
      success: true,
      message: "User read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: Error.message,
    });
  }
};

// export let readSpecificUserByName = async (req,res)=>{
//     try {
//         let result = await User.find({name:"aavash"});
//        res.json({
//         success:true,
//         message:"User read successfully",
//         data:result,
//     })
//        } catch (error) {
//         res.json({
//             success:false,
//             message: Error.message
//         })
//        }
// }

export let deleteUser = async (req, res) => {
  let id = req.params.id;

  try {
    let result = await User.findByIdAndDelete(id);

    if (result === null) {
      res.json({
        success: false,
        message: "Data not available to delete",
      });
    } else {
      res.json({
        success: true,
        message: "User delete successfully",
        data: result,
      });
    }
  } catch (error) {
    res.json({
      success: false,
      message: Error.message,
    });
  }
};

export let updateUser = async (req, res) => {
  let id = req.params.id;
  let data = req.body;

  try {
    let result = await User.findByIdAndUpdate(id, data, { new: true });

    res.json({
      success: true,
      message: "User updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let LoginUser = async (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  console.log(email)
  console.log(password)

  try {
    let user = await User.findOne({ email: email });
    let databasePassword = user.password;

    let isValidPassword = await bcrypt.compare(password, databasePassword);
    console.log(isValidPassword)
    if (isValidPassword) {
        
      res.json({
        success: true,
        message: "Logged in succesfully",
      });
    } else {
      res.json({
        success: false,
        message: "Email or password doesnot match",
      });
    }
  } catch (error) {
    res.json({
      success: false,
      message: "Email or password doesnot match",
    });
  }
};
