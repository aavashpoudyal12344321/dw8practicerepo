import { Product } from "../schema/model.js";

export let createProduct =  async (req,res)=>{
    let data =  req.body;
    try {
     let result = await Product.create(data);
    res.json({
     success:true,
     message:"Product added successfully",
     data:result,
 })  
    } catch (error) {
     res.json({
         success:false,
         message: Error.message
     })
    } 
 }

 export let readAllProduct = async (req,res)=>{
    // let data =  req.body;
   try {

    let result = await Product.find({});
   res.json({
    success:true,
    message:"Product read successfully",
    data:result,
})
   } catch (error) {
    res.json({
        success:false,
        message: error.message
    })
   }
}

export let readSpecificProduct = async (req,res)=>{
    let id = req.params.id
    try {
        let result = await Product.findById(id);
       res.json({
        success:true,
        message:"Product read successfully",
        data:result,
    })
       } catch (error) {
        res.json({
            success:false,
            message: Error.message
        })
       }
}

// export let readSpecificProductByName = async (req,res)=>{
//     try {
//         let result = await Product.find({name:"aavash"});
//        res.json({
//         success:true,
//         message:"Product read successfully",
//         data:result,
//     })
//        } catch (error) {
//         res.json({
//             success:false,
//             message: Error.message
//         })
//        }
// }

export let deleteProduct = async (req,res)=>{
    let id = req.params.id
    
    try {
        let result = await Product.findByIdAndDelete(id)

        if (result===null){
            res.json({
                success:false,
                message:"Data not available to delete"
            })
        }
        else{
            res.json({
                success:true,
            message:"Product delete successfully",
            data:result,
            })

        }
        
    } catch (error) {
        res.json({
            success:false,
            message: Error.message
        })
        
    }
}


export let updateProduct = async (req,res)=>{
    let id = req.params.id
    let data = req.body
    
    try {
        let result = await Product.findByIdAndUpdate(id, data, {new:true})

       
            res.json({
                success:true,
            message:"Product updated successfully",
            data:result,
            })

        
        
    } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
        
    }
}


