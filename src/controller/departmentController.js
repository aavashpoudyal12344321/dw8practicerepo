import { Department } from "../schema/model.js";

export let createDepartment =  async (req,res)=>{
    let data =  req.body;
    try {
     let result = await Department.create(data);
    res.json({
     success:true,
     message:"Department added successfully",
     data:result,
 })  
    } catch (error) {
     res.json({
         success:false,
         message: error.message
     })
    } 
 }

 export let readAllDepartment = async (req,res)=>{
    let data =  req.body;
   try {
    let result = await Department.find({});
   res.json({
    success:true,
    message:"Department read successfully",
    data:result,
})
   } catch (error) {
    res.json({
        success:false,
        message: error.message
    })
   }
}

export let readSpecificDepartment = async (req,res)=>{
    let id = req.params.id
    try {
        let result = await Department.findById(id);
       res.json({
        success:true,
        message:"Department read successfully",
        data:result,
    })
       } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
       }
}

export let UpdateDepartment = async (req,res)=>{
    let id= req.params.id
    let data = req.body

    try {
        let result =await Department.findByIdAndUpdate(id,data, {new:true})
            res.json({
                success:true,
                message:"Department updated successfully",
                data:result,
            })
        
        } catch (error) {
        res.json({
            success:false,
            message: error.message,
        })
    }
}

export let deleteDepartment =async (req,res)=>{
    let id = req.params.id
    try {
        let result =await Department.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"Department deleted successfully",
            data:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    } 
}