
export let createClassRoom =  async (req,res)=>{
    let data =  req.body;
    try {
     let result = await ClassRoom.create(data);
    res.json({
     success:true,
     message:"ClassRoom added successfully",
     data:result,
 })  
    } catch (error) {
     res.json({
         success:false,
         message: error.message
     })
    } 
 }

 export let readAllClassRoom = async (req,res)=>{
    let data =  req.body;
   try {
    let result = await ClassRoom.find({});
   res.json({
    success:true,
    message:"ClassRoom read successfully",
    data:result,
})
   } catch (error) {
    res.json({
        success:false,
        message: error.message
    })
   }
}

export let readSpecificClassRoom = async (req,res)=>{
    let id = req.params.id
    try {
        let result = await ClassRoom.findById(id);
       res.json({
        success:true,
        message:"ClassRoom read successfully",
        data:result,
    })
       } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
       }
}

export let UpdateClassRoom = async (req,res)=>{
    let id= req.params.id
    let data = req.body

    try {
        let result =await ClassRoom.findByIdAndUpdate(id,data, {new:true})
            res.json({
                success:true,
                message:"ClassRoom updated successfully",
                data:result,
            })
        
        } catch (error) {
        res.json({
            success:false,
            message: error.message,
        })
    }
}

export let deleteClassRoom =async (req,res)=>{
    let id = req.params.id
    try {
        let result =await ClassRoom.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"ClassRoom deleted successfully",
            data:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    } 
}