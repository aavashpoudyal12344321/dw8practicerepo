import { Teacher } from "../schema/model.js";

export let createTeacher =  async (req,res)=>{
    let data =  req.body;
    try {
     let result = await Teacher.create(data);
    res.json({
     success:true,
     message:"Teacher added successfully",
     data:result,
 })  
    } catch (error) {
     res.json({
         success:false,
         message: error.message
     })
    } 
 }

 export let readAllTeacher = async (req,res)=>{
    let data =  req.body;
   try {
    let result = await Teacher.find({});
   res.json({
    success:true,
    message:"Teacher read successfully",
    data:result,
})
   } catch (error) {
    res.json({
        success:false,
        message: error.message
    })
   }
}

export let readSpecificTeacher = async (req,res)=>{
    let id = req.params.id
    try {
        let result = await Teacher.findById(id);
       res.json({
        success:true,
        message:"Teacher read successfully",
        data:result,
    })
       } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
       }
}

export let updateTeacher = async (req,res)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Teacher.findByIdAndUpdate(id, data, {new:true})
        res.json({
            success:true,
            message:"Teacher updated successfully",
            data: result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteTeacher = async (req,res)=>{
    let id = req.params.id
    try {
        let result = await Teacher.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"Teacher deleted successfully",
            data: result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}