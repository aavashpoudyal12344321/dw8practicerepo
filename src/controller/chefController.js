import { Chef } from "../schema/model.js"

export let createChef =async (req,res)=>{
        let data = req.body
            try {
                let result = await Chef.create(data)
                res.json({
                    success:true,
                    message:"Chef data created succesfully",
                    data:result,
                })
            } catch (error) {
                res.json({
                    success:false,
                    message:error.message
                })
                
            }
        }

export let readAllChef = async (req,res)=> {
    let data = req.body
        try {
            let result = await Chef.find({})
            res.json({
                success:true,
                message:"Chef read succesfully",
                data:result
            })
        } catch (error) {
            res.json({
                success:false,
                message:error.message
            })
            
        }
}


export let readSpecificChef = async (req,res)=> {
    let id = req.params.id
        try {
            let result = await Chef.findById(id)
            res.json({
                success:true,
                message:"Chef read succesfully",
                data:result
            })
        } catch (error) {
            res.json({
                success:false,
                message:error.message
            })
            
        }
}


export let updateChef = async (req,res)=> {
    let data = req.body
    let id = req.params.id
        try {
            let result = await Chef.findByIdAndUpdate(id, data, {new:true})
            res.json({
                success:true,
                message:"Chef updated succesfully",
                data:result
            })
        } catch (error) {
            res.json({
                success:false,
                message:error.message
            })
            
        }
}

export let deleteChef = async (req,res)=> {
    let id = req.params.id
        try {
            let result = await Chef.findByIdAndDelete(id)
            res.json({
                success:true,
                message:"Chef deleted succesfully",
                data:result
            })
        } catch (error) {
            res.json({
                success:false,
                message:error.message
            })
            
        }
}